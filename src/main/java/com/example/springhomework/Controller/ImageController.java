package com.example.springhomework.Controller;


import com.example.springhomework.Model.Pictures;
import com.example.springhomework.Service.InfromService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


@Controller
public class ImageController {
    int i;
    private InfromService infromService;
    @Autowired
    public ImageController(InfromService infromService){
        this.infromService = infromService;
    }

    @GetMapping("/")
    public String openHome(ModelMap modelMap){
        String str = "hello";

        modelMap.addAttribute("arrimage",infromService.getAllArrimage());
        //System.out.println(infromService.getAllArrimage());
        return "index";
    }

    @GetMapping("/add")
    public String openAdd(){
        return "add";
    }

    //create inform
    @PostMapping("/post/add")
    public String addImage(@ModelAttribute Pictures pictures){

        i = i+1;
        pictures.setId(i);
        infromService.getAllArrimage().add(pictures);

        return "redirect:/";
    }


    //delete item
    @GetMapping("/delete/{id}")
    public String deleteImage(@PathVariable (name = "id") int id){

        infromService.deleteInform(id);
        return "redirect:/";
    }

    //view item
    @GetMapping("/view/{id}")
    public String viewImage(@PathVariable (name = "id") int id,ModelMap modelMap){

        Pictures infrom = infromService.findOne(id);
        modelMap.addAttribute("infrom",infrom);

        return "view";
    }

    @GetMapping("/getupdate/{id}")
    public String updateImage(@PathVariable (name = "id") int id, Model model){

        Pictures pictures = infromService.findOne(id);
        model.addAttribute("picture",pictures);

        return "update_image";
    }

    @PostMapping("/update/{id}")
    public String updateImage(@PathVariable (name = "id") int id,@ModelAttribute Pictures pictures){

        for (Pictures pictures1: infromService.getAllArrimage()) {
            if (pictures1.getId() == id){
                pictures1.setTitle(pictures.getTitle());
                pictures1.setDis(pictures.getDis());
            }
        }

        return "redirect:/";
    }

}
