package com.example.springhomework.Model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pictures {
    private int id;
    private String title;
    private String dis;
    private String photo;
}
