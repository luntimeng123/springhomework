package com.example.springhomework.Repostory.Reposi;


import com.example.springhomework.Model.Pictures;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class INformRepsitory {

    private List<Pictures> arrimage = new ArrayList<>();

    //returen array image ,it = arrimage
    public List<Pictures> getAllArrimage() {
        return arrimage;
    }

    //show only one item of arrimage by id
    public Pictures findOne(int id){

        Pictures infrom1 = null;

        for (Pictures infrom:arrimage) {
            if (infrom.getId() == id){
                infrom1 = infrom;
            }
        }

        return infrom1;
    }

    //add new item
    public void addNewInform(Pictures infrom){
        arrimage.add(infrom);
    }

    //delete item
    public void deleteInform(int id){
        for (Pictures infrom:arrimage) {
            if (infrom.getId() == id){
                arrimage.remove(infrom);
            }
        }
    }

}
