package com.example.springhomework.Service;


import com.example.springhomework.Model.Pictures;
import org.springframework.stereotype.Service;

import java.util.List;


// service create for handle on relation between controller and Respository
@Service
public interface InfromService {

    public List<Pictures> getAllArrimage();
    public Pictures findOne(int id);
    public void addNewInform(Pictures infrom);
    public void deleteInform(int id);

}
