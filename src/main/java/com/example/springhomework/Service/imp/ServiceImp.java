package com.example.springhomework.Service.imp;

import com.example.springhomework.Model.Pictures;
import com.example.springhomework.Repostory.Reposi.INformRepsitory;
import com.example.springhomework.Service.InfromService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceImp implements InfromService {

    //need it to work with resource like collection or database
    private INformRepsitory informrepsitory;

@Autowired
    public ServiceImp(INformRepsitory informrepsitory){
        this.informrepsitory = informrepsitory;
    }

    @Override
    public List<Pictures> getAllArrimage() {
        return informrepsitory.getAllArrimage();
    }

    @Override
    public Pictures findOne(int id) {
        return informrepsitory.findOne(id);
    }

    @Override
    public void addNewInform(Pictures infrom) {
        informrepsitory.addNewInform(infrom);
    }

    @Override
    public void deleteInform(int id) {
        try {
            informrepsitory.deleteInform(id);
        }catch (Exception e){
            System.out.println(e + "give correct id");
        }
    }

}
